USE m1u2h7;

/*
  APARTADO 1:
*/

  SELECT 
    p.`C�DIGO ART�CULO`, p.SECCI�N, p.PRECIO
  FROM productos p
  WHERE p.PRECIO>100;

  CREATE OR REPLACE VIEW CONSULTA1 AS
    SELECT 
      p.`C�DIGO ART�CULO`, p.SECCI�N, p.PRECIO
    FROM productos p
    WHERE p.PRECIO>100;

  SELECT * FROM CONSULTA1 c;

  /*
    APARTADO 2:
  */

    ALTER VIEW CONSULTA1 AS
      SELECT 
        p.`C�DIGO ART�CULO`, p.SECCI�N, p.PRECIO
      FROM productos p
      WHERE p.PRECIO>100 
      ORDER BY p.PRECIO DESC;


/*
  APARTADO 3:
*/

  SELECT 
    c.`C�DIGO ART�CULO`, c.`SECCI�N`,C.`PRECIO` 
  FROM CONSULTA1 c 
  WHERE c.`SECCI�N`='DEPORTES';

  CREATE OR REPLACE VIEW consulta2 AS
    SELECT 
      c.`C�DIGO ART�CULO`, c.`SECCI�N`,C.`PRECIO` 
    FROM CONSULTA1 c 
    WHERE c.`SECCI�N`='DEPORTES';


  /*
    APARTADO 5:
 */

    INSERT INTO CONSULTA1 VALUES ('AR90','NOVEDADES',5);

    SELECT * FROM productos p;

/*
  APARTADO 6:
*/
  ALTER VIEW CONSULTA1
    AS SELECT 
        p.`C�DIGO ART�CULO`, p.SECCI�N, p.PRECIO
      FROM productos p
      WHERE p.PRECIO>100 
      ORDER BY p.PRECIO DESC
      WITH LOCAL CHECK OPTION;

/*
  APARTADO 7:
*/
INSERT INTO CONSULTA1 VALUES ('AR91', 'NOVEDADES',5);

/* Falla el CHECK OPTION porque es precio>100 e introduzco un producto de precio=5
*/

  INSERT INTO CONSULTA1 VALUES ('AR91', 'NOVEDADES',110);

/*
  APARTADO 8:
 */
 -- Insertar en productos a trav�s de la vista CONSULTA2
INSERT INTO consulta2 VALUES ('AR92','NOVEDADES',5);

/*
  APARTADO 9:
*/
  ALTER VIEW consulta2 AS
    SELECT 
      c.`C�DIGO ART�CULO`, c.`SECCI�N`,C.`PRECIO` 
    FROM CONSULTA1 c 
    WHERE c.`SECCI�N`='DEPORTES'
    WITH LOCAL CHECK OPTION;

SELECT * FROM consulta2 c;

/*
  APARTADO 10:
*/
-- Insertar en productos a trav�s de la vista CONSULTA2
INSERT INTO consulta2 VALUES ('AR93','NOVEDADES',5);

-- Falla el CHECK OPTION porque el producto es de NOVEDADES y la consulta2 muestra los productos de DEPORTES
INSERT INTO consulta2 VALUES ('AR93','DEPORTES',5);

SELECT * FROM consulta2 c;
SELECT * FROM productos;

/*
  APARTADO 11:
*/
ALTER VIEW consulta2 AS
  SELECT 
      c.`C�DIGO ART�CULO`, c.`SECCI�N`,C.`PRECIO` 
    FROM CONSULTA1 c 
    WHERE c.`SECCI�N`='DEPORTES'
  WITH CASCADED CHECK OPTION;

/*
  APARTADO 12:
*/
INSERT INTO consulta2 VALUES ('AR94','DEPORTES',5);

-- Falla al introducir el dato por el CHECK OPTION porque el precio debe ser >100 al ser una vista sobre consulta1 que tiene su propia restriccion de precio
INSERT INTO consulta2 VALUES ('AR94','DEPORTES',200);